import {Component, OnDestroy, OnInit} from '@angular/core';
import {SensorService} from './sensor.service';
import {AuthService} from '../auth/auth.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {environment} from '../../environments/environment';
import {Subscription} from 'rxjs';
import {SensorDataModel} from './sensor-data.model';

const username = environment.apiUser;
const passwd = environment.apiPasswd;

@Component({
  selector: 'app-charts',
  templateUrl: './sensors.component.html',
  styleUrls: ['./sensors.component.css']
})
export class SensorsComponent implements OnInit, OnDestroy {
  sensors: SensorDataModel[] = [];
  dashboardForm: FormGroup;
  filterForm: FormGroup;
  isLoading = false;
  isDisabled = true;
  device: string;
  aggrePeriod: number;
  dateStart: Date;
  dateEnd: Date;
//  invalidDateRange: boolean;
  deviceEntries = ['BAI_02', 'BAI_01', 'BAI_5e224b7b016ac40001b6a5ee', 'BAI_5e220210016ac40001b6a5a2'];
  sensorList: string[] = [];
  sensorOrType: string;
  filteredDashboardDataBySensor: object[] = [];

  aggPeriods = [
    {label: '15 minutes', value: 15},
    {label: '60 minutes', value: 60},
    {label: '24 hours', value: 1440}
  ];
  chartTypes = [{name: 'Line', link: 'line'}, {name: 'Bar', link: 'bar'}, {name: 'Pie', link: 'pie'}];
  private sensorDataSub: Subscription;
  private authStatusSub: Subscription;
  private selSensorDataSub: Subscription;

  constructor(private authService: AuthService, private sensorService: SensorService) { }

  ngOnInit(): void {
    this.authService.login(username, passwd);
    this.authStatusSub = this.authService.getAuthStatusListener().subscribe();
    this.dashboardForm = new FormGroup({
      dateTimeRange: new FormGroup({
        dateTimeStart: new FormControl(null,
          [
            Validators.required,
            Validators.max(this.dashboardForm?.value.dateTimeRange.dateTimeEnd),
            this.isInvalidDateRange.bind(this)]
        ),
        dateTimeEnd: new FormControl(null,
          [
            Validators.required,
            Validators.min(this.dashboardForm?.value.dateTimeRange.dateTimeStart),
            this.isInvalidDateRange.bind(this)]
        )
      }),
      deviceEntry: new FormControl('BAI_02', Validators.required),
      aggregation: new FormControl(60),
    });

    this.filterForm = new FormGroup({
      sensorsOrType: new FormControl(null),
      sensorItems: new FormControl(null),
    });

  }

  onGetData() {
    this.isLoading = true;
    this.device = this.dashboardForm.value.deviceEntry;
    this.aggrePeriod = this.dashboardForm.value.aggregation;
    this.dateStart = this.dashboardForm.value.dateTimeRange.dateTimeStart.toJSON().split('.')[0];
    this.dateEnd = this.dashboardForm.value.dateTimeRange.dateTimeEnd.toJSON().split('.')[0];
    // if (!this.sensors.length) {
    this.sensorService.getAPIData(this.device, this.aggrePeriod, this.dateStart, this.dateEnd);
    // }
    this.sensorDataSub = this.sensorService.getSensorDataUpdateListener()
      .subscribe((sensorData: {sensors: SensorDataModel[]}) => {
        this.sensors = sensorData.sensors;
        if (this.sensors.length > 0) {
          this.isLoading = false;
          this.isDisabled = false;
          const countSensors = this.sensorService.filterByKey(this.sensors, 'count');

          console.log('countSensors', JSON.stringify(countSensors));
        }
      });
  }

  changeSensorType() {
    this.sensorOrType = this.filterForm.value.sensorsOrType;
    switch (this.sensorOrType) {
      case 'count':
        this.sensorList = this.sensorService.filterByKey(this.sensors, 'count');
        break;
      case 'speed':
        this.sensorList = this.sensorService.filterByKey(this.sensors, 'speed');
        break;
      case 'presence':
        this.sensorList = this.sensorService.filterByKey(this.sensors, 'presence');
    }
  }

  onGetFilteredData() {
   const selectedSensors = this.filterForm.value.sensorItems;
   this.sensorService.getFilteredData(selectedSensors);
  }

  ngOnDestroy(): void {
    this.authStatusSub.unsubscribe();
    this.sensorDataSub.unsubscribe();
    this.selSensorDataSub.unsubscribe();
  }

  isInvalidDateRange(): {[s: string]: boolean} {
    const startDate = new Date(this.dateStart).getTime();
    const endDate = new Date(this.dateEnd).getTime();
    const rangeBetweenStartAndEnd = (endDate - startDate);

    if (startDate > endDate || (rangeBetweenStartAndEnd > 86400000)) {
      return { invalidDateRange: true };
    }
    return null;
  }
}
