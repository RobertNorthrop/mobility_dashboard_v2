import { Component, OnInit } from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import { Label } from 'ng2-charts';
import {SensorDataModel} from '../../sensor-data.model';
import {Subscription} from 'rxjs';
import {SensorService} from '../../sensor.service';

@Component({
  selector: 'app-bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.css']
})
export class BarChartComponent implements OnInit {
  chartData: SensorDataModel[] = [];
  private barChartSub: Subscription;
  private carNorth: number[] = [];
  private carSouth: number[] = [];
  private personNorth: number[];
  private personSouth: number[];
  private bicycleNorth: number[];
  private bicycleSouth: number[];

  public barChartOptions: ChartOptions = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: { xAxes: [{}], yAxes: [{}] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      }
    }
  };
  public barChartLabels: Label[];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartPlugins = [pluginDataLabels];

  public barChartData: ChartDataSets[];

  constructor(private sensorService: SensorService) { }

  ngOnInit() {
    this.barChartSub = this.sensorService.getSensorDataUpdateListener()
      .subscribe((chartData: {sensors: SensorDataModel[]}) => {
        this.chartData = chartData.sensors;

        const sensorCountKeys = this.sensorService.filterByKey(this.chartData, 'count');

        const newFilteredDates = this.chartData.reduce((acc, item) => {
          return [...acc, item.time_period];
        }, []);
        this.barChartLabels = [...newFilteredDates];

        const carNorthArr = this.chartData.map((chartItem) => chartItem.car_north_count);
        this.carNorth = [...carNorthArr];

        const carSouthArr = this.chartData.map((chartItem) => chartItem.car_south_count);
        this.carSouth = [...carSouthArr];

        const personNorthArr = this.chartData.map((chartItem) => chartItem.person_north_count);
        this.personNorth = [...personNorthArr];

        const personSouthArr = this.chartData.map((chartItem) => chartItem.person_south_count);
        this.personSouth = [...personSouthArr];

        // const bicycleNorthArr = this.chartData.map((chartItem) => chartItem.bicycle_north_count);
        // this.bicycleNorth = [...bicycleNorthArr];
        //
        // const bicycleSouthArr = this.chartData.map((chartItem) => chartItem.bicycle_south_count);
        // this.bicycleSouth = [...bicycleSouthArr];

        this.barChartData = [
          { data: this.carNorth, label: sensorCountKeys[0] },
          { data: this.carSouth, label: sensorCountKeys[1] },
          { data: this.personNorth, label: sensorCountKeys[2] },
          { data: this.personSouth, label: sensorCountKeys[3] },
          // { data: this.bicycleNorth, label: sensorCountKeys[4] },
          // { data: this.bicycleSouth, label: sensorCountKeys[5] },
        ];
      });
  }

  // events
  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }
}
