import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {SensorService} from '../../sensor.service';
import {SensorDataModel} from '../../sensor-data.model';

import { ChartDataSets, ChartOptions } from 'chart.js';
import { Color, Label } from 'ng2-charts';
import {Router} from '@angular/router';

@Component({
  selector: 'app-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.css']
})
export class LineChartComponent implements OnInit, OnDestroy {
  chartData: SensorDataModel[] = [];
  private lineChartSub: Subscription;
  private carNorth: number[] = [];
  private carSouth: number[] = [];
  private personNorth: number[];
  private personSouth: number[];
  private bicycleNorth: number[];
  private bicycleSouth: number[];

  public lineChartData: ChartDataSets[];
  public lineChartLabels: Label[];
  public lineChartOptions: ChartOptions = {
    responsive: true,
  };
  public lineChartColors: Color[] = [
    {
      borderColor: 'black',
      // backgroundColor: 'rgba(255,0,0,0.3)',
    },
  ];
  public lineChartLegend = true;
  public lineChartType = 'line';
  public lineChartPlugins = [];

  constructor(private sensorService: SensorService, private route: Router){}

  ngOnInit(): void {
    this.lineChartSub = this.sensorService.getSensorDataUpdateListener()
      .subscribe((chartData: {sensors: SensorDataModel[]}) => {
        this.chartData = chartData.sensors;
        // // create an array of unique keys
        // const uniqueKeys = Object.keys(this.chartData.reduce((result, obj) => {
        //   return Object.assign(result, obj);
        // }, {}));
        // // create an array of each of the sensor keys
        // const sensorCountKeys = uniqueKeys.filter((item) => item.includes('count'));

        const sensorCountKeys = this.sensorService.filterByKey(this.chartData, 'count');

        const newFilteredDates = this.chartData.reduce((acc, item) => {
          return [...acc, item.time_period];
        }, []);
        this.lineChartLabels = [...newFilteredDates];

        const carNorthArr = this.chartData.map((chartItem) => chartItem.car_north_count);
        this.carNorth = [...carNorthArr];

        const carSouthArr = this.chartData.map((chartItem) => chartItem.car_south_count);
        this.carSouth = [...carSouthArr];

        const personNorthArr = this.chartData.map((chartItem) => chartItem.person_north_count);
        this.personNorth = [...personNorthArr];

        const personSouthArr = this.chartData.map((chartItem) => chartItem.person_south_count);
        this.personSouth = [...personSouthArr];

        // const bicycleNorthArr = this.chartData.map((chartItem) => chartItem.bicycle_north_count);
        // this.bicycleNorth = [...bicycleNorthArr];
        //
        // const bicycleSouthArr = this.chartData.map((chartItem) => chartItem.bicycle_south_count);
        // this.bicycleSouth = [...bicycleSouthArr];

        this.lineChartData = [
          { data: this.carNorth, label: sensorCountKeys[0] },
          { data: this.carSouth, label: sensorCountKeys[1] },
          { data: this.personNorth, label: sensorCountKeys[2] },
          { data: this.personSouth, label: sensorCountKeys[3] },
          // { data: this.bicycleNorth, label: sensorCountKeys[4] },
          // { data: this.bicycleSouth, label: sensorCountKeys[5] },
        ];
      });
  }

  ngOnDestroy(): void {
    this.lineChartSub.unsubscribe();
  }
}
