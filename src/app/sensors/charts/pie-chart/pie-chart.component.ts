import { Component, OnInit } from '@angular/core';
import { ChartType, ChartOptions } from 'chart.js';
import { Label } from 'ng2-charts';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import {SensorService} from '../../sensor.service';
import {Subscription} from 'rxjs';
import {SensorDataModel} from '../../sensor-data.model';

@Component({
  selector: 'app-pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.css']
})
export class PieChartComponent implements OnInit {
  private chartSub: Subscription;
  // Pie
  public pieChartOptions: ChartOptions = {
    responsive: true,
    legend: {
      position: 'top',
    },
    plugins: {
      datalabels: {
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          return label;
        },
      },
    }
  };
//  public pieChartLabels: Label[] = [['Download', 'Sales'], ['In', 'Store', 'Sales'], 'Mail Sales'];
  pieChartLabels: Label[] = [];
//  public pieChartData = [300, 500, 100];
  chartData: SensorDataModel[] = [];
  pieChartData: number[];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [pluginDataLabels];
  public pieChartColors = [
    {
      backgroundColor: ['rgba(255,0,0,0.3)', 'rgba(0,255,0,0.3)', 'rgba(0,0,255,0.3)'],
    },
  ];

  constructor(private sensorService: SensorService) { }

  ngOnInit() {
    this.chartSub = this.sensorService.getSensorDataUpdateListener()
      .subscribe((chartData) => {
        this.chartData = chartData.sensors;

        const uniqueKeys = Object.keys(this.chartData.reduce((result, obj) => {
          return Object.assign(result, obj);
        }, {}));

        const sensorCountKeys = uniqueKeys.filter((item) => item.includes('count'));
        console.log('sensorCountKeys', JSON.stringify(sensorCountKeys));

        this.pieChartLabels = [...sensorCountKeys];

        const carNorthArr = this.chartData.map((chartItem) => chartItem.car_north_count);
        const carNorthArrSum: number = this.sumSensorCountVals(carNorthArr);

        const carSouthArr = this.chartData.map((chartItem) => chartItem.car_south_count);
        const carSouthArrSum: number = this.sumSensorCountVals(carSouthArr);

        const personNorthArr = this.chartData.map((chartItem) => chartItem.person_north_count);
        const personNorthArrSum = this.sumSensorCountVals(personNorthArr);

        const personSouthArr = this.chartData.map((chartItem) => chartItem.person_south_count);
        const personSouthArrSum = this.sumSensorCountVals(personSouthArr);

        this.pieChartData = [carNorthArrSum, carSouthArrSum, personNorthArrSum, personSouthArrSum];


        // this.pieChartData = pieChartData.reduce((prevCountVal, currCountVal) => {
        //   sensorCountKeys.forEach(key => {
        //     const newVal = prevCountVal[key] + currCountVal[key];
        //     return [...newVal];
        //
        //
        //   });
        // });


      });
  }

  // events
  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  private sumSensorCountVals(arr) {
    return arr.reduce((a, b) => a + b, 0);
  }

  // addSlice() {
  //   this.pieChartLabels.push(['Line 1', 'Line 2', 'Line 3']);
  //   this.pieChartData.push(400);
  //   this.pieChartColors[0].backgroundColor.push('rgba(196,79,244,0.3)');
  // }
  //
  // removeSlice() {
  //   this.pieChartLabels.pop();
  //   this.pieChartData.pop();
  //   this.pieChartColors[0].backgroundColor.pop();
  // }
  //
  // changeLegendPosition() {
  //   this.pieChartOptions.legend.position = this.pieChartOptions.legend.position === 'left' ? 'top' : 'left';
  // }
}
