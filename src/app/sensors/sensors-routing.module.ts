import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';

import {LineChartComponent} from './charts/line-chart/line-chart.component';
import { BarChartComponent } from './charts/bar-chart/bar-chart.component';
import { PieChartComponent} from './charts/pie-chart/pie-chart.component';
import { BoxPlotComponent } from './charts/box-plot/box-plot.component';

const routes: Routes = [
  { path: 'line', component: LineChartComponent },
  { path: 'bar', component: BarChartComponent },
  { path: 'pie', component: PieChartComponent},
  { path: 'box', component: BoxPlotComponent }
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule]
})
export class SensorsRoutingModule {
}
