import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';

import {HttpClientModule} from '@angular/common/http';
import { AngularMaterialModule } from '../angular-material.module';

import { SensorsComponent } from './sensors.component';
import { LineChartComponent } from './charts/line-chart/line-chart.component';
import { BoxPlotComponent } from './charts/box-plot/box-plot.component';
import { PieChartComponent } from './charts/pie-chart/pie-chart.component';
import { BarChartComponent } from './charts/bar-chart/bar-chart.component';

import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {SensorsRoutingModule} from './sensors-routing.module';
import {ChartsModule} from 'ng2-charts';
import {MatRadioModule} from '@angular/material/radio';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {TableReportComponent} from './table-report/table-report.component';

@NgModule({
  declarations: [
    SensorsComponent,
    LineChartComponent,
    BoxPlotComponent,
    PieChartComponent,
    BarChartComponent,
    TableReportComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    HttpClientModule,
    AngularMaterialModule,
    MatFormFieldModule,
    MatSelectModule,
    SensorsRoutingModule,
    ChartsModule,
    MatRadioModule,
    MatDatepickerModule,
  ]
})
export class SensorsModule { }
