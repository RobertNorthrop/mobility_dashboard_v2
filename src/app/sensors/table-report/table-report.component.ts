import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';

import { SensorDataModel} from '../sensor-data.model';
import {Subscription} from 'rxjs';
import {SensorService} from '../sensor.service';



@Component({
  selector: 'app-table-report',
  templateUrl: './table-report.component.html',
  styleUrls: ['./table-report.component.css']
})
export class TableReportComponent implements OnInit, OnDestroy {
  displayedColumns: string[] = [
    'time_period',
    'car_north_count',
    'car_south_count',
    'person_north_count',
    'person_south_count'
  ];
  SENSOR_DATA: SensorDataModel[] = [];
  dataSource: any;
  dataSub: Subscription;

  constructor(private sensorService: SensorService) {}

  @ViewChild(MatSort, {static: true}) sort: MatSort;

  ngOnInit() {
    this.dataSub = this.sensorService.getSensorDataUpdateListener()
      .subscribe((gridData) => {
        this.SENSOR_DATA = gridData.sensors;
        this.dataSource = new MatTableDataSource(this.SENSOR_DATA);
        this.dataSource.sort = this.sort;
      });
  }

  ngOnDestroy() {
    this.dataSub.unsubscribe();
  }
}
