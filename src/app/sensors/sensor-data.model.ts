export interface SensorDataModel {
  ts: Date;
  time_period: string;
  car_north_count: number;
  car_south_count: number;
  person_north_count: number;
  person_south_count: number;
  bicycle_north_count: number;
  bicycle_south_count: number;
  bus_north_count: number;
  bus_south_count: number;
  truck_north_count: number;
  truck_south_count: number;
  car_north_speed: number;
  car_south_speed: number;
}
