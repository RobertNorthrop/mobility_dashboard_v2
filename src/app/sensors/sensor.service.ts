import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { environment } from '../../environments/environment';
import { AuthService } from '../auth/auth.service';
import {map} from 'rxjs/operators';
import {BehaviorSubject, Subject} from 'rxjs';
import {SensorDataModel} from './sensor-data.model';

const apiUrl = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class SensorService {
  private sensors: SensorDataModel[] = [];
  private selSensors = [];
  sensorDataSubject = new BehaviorSubject<{sensors: SensorDataModel[]}>({sensors: this.sensors});
  selectSensorsDataSubject = new BehaviorSubject<{selSensors: SensorDataModel[]}>({selSensors: this.sensors});

  constructor(private http: HttpClient) { }

  getAPIData(deviceEntry: string, aggPeriod: number, startDate: Date, endDate: Date) {
    const URL = `${deviceEntry}/${aggPeriod}/json`;
    const qParams = `?start_time=${startDate}&end_time=${endDate}&kind=all`;
    this.http.get(apiUrl + URL + qParams)
      .pipe(map((rawSensorData) => {
        const report = 'report';
        // create new array contain raw sensor data objects
        const sensorData = Object.keys(rawSensorData[report])
          .map(key => rawSensorData[report]);

        return sensorData.map((sensorItems, index) => {
          const dateTSKey = Object.keys(sensorItems)[index];
          const dateTS =  new Date(dateTSKey);
          return {
            ts: dateTS,
            time_period: dateTS.toLocaleTimeString(),
            car_north_count: sensorItems[dateTSKey].COUNT_CAR_1_North,
            car_south_count: sensorItems[dateTSKey].COUNT_CAR_1_South,
            person_north_count: sensorItems[dateTSKey].COUNT_PERSON_1_North,
            person_south_count: sensorItems[dateTSKey].COUNT_PERSON_1_South,
            bicycle_north_count: sensorItems[dateTSKey].COUNT_BICYCLE_1_North,
            bicycle_south_count: sensorItems[dateTSKey].COUNT_BICYLE_1_South,
            bus_north_count: sensorItems[dateTSKey].COUNT_BUS_1_North,
            bus_south_count: sensorItems[dateTSKey].COUNT_BUS_1_South,
            truck_north_count: sensorItems[dateTSKey].COUNT_TRUCK_1_North,
            truck_south_count: sensorItems[dateTSKey].COUNT_TRUCK_1_South,
            car_north_speed: sensorItems[dateTSKey].SPEED_CAR_1_North,
            car_south_speed: sensorItems[dateTSKey].SPEED_CAR_1_South,
          };

        });
      }))
      .subscribe((results) => {
        this.sensors = results;
        this.sensors.pop();
        this.sensorDataSubject.next({sensors: [...this.sensors]});
      });
  }

  getSensorDataUpdateListener() {
    return this.sensorDataSubject.asObservable();
  }

  getSelSensorDataListener() {
    return this.selectSensorsDataSubject.asObservable();
  }

  getFilteredData(selectedSensors) {
    // const selectedSensors = this.filterForm.value.sensorItems;

    this.sensors.map(item => {
      const filteredData = Object.keys(item)
        .filter(key => selectedSensors.includes(key))
        .reduce((obj, key) => {
          const timePeriod = 'time_period';
          const ts = 'ts';
          obj[ts] = item.ts;
          obj[timePeriod] = item.time_period;
          obj[key] = item[key];

          return obj;
        }, {});

      this.selSensors.push(filteredData);
      this.selectSensorsDataSubject.next({selSensors: [...this.selSensors]});

      console.log('selSensors', JSON.stringify(this.selSensors));
    });
  }

  // Creates an array of either the count sensor or speed or presence
  filterByKey(dataArr, keyMatch) {
    const uniqueKeys = Object.keys(dataArr.reduce((result, obj) => {
      return Object.assign(result, obj);
    }, {}));
    // create an array of each of the sensor keys
    return uniqueKeys.filter((item) => item.includes(keyMatch));
  }



}
