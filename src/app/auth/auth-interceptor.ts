import {HttpInterceptor, HttpRequest, HttpHandler} from '@angular/common/http';
import { Injectable } from '@angular/core';

/** authentication credentials for Dev purposes only.
* For Prod, the username and password will have to be captured when the user logs in
**/
import { AuthService } from './auth.service';
import {environment} from '../../environments/environment';

const username = environment.apiUser;
const passwd = environment.apiPasswd;

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    const authRequest = req.clone({
      headers: req.headers.set('Authorization', 'Basic ' + btoa(username + ':' + passwd))
    });
    console.log('authRequest', authRequest);
    return next.handle(authRequest);
  }

}
