import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SensorsComponent } from './sensors/sensors.component';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {AuthInterceptor} from './auth/auth-interceptor';


const routes: Routes = [
  { path: '', component: SensorsComponent },
  { path: 'sensors',
    component: SensorsComponent,
    loadChildren: () => import('./sensors/sensors.module')
      .then(m => m.SensorsModule)
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
  ],
})
export class AppRoutingModule { }
